(defproject airtimesender "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/data.json "0.2.5"]
                 [org.clojure/tools.logging "0.2.6"]
                 [c3p0/c3p0 "0.9.1.2"]
                 [org.clojure/java.jdbc "0.3.6"]
                 [org.postgresql/postgresql "9.3-1100-jdbc41"]
                 ;;expose endpoints
                 [http-kit "2.2.0"]
                 [ring/ring-headers "0.2.0"]
                 [ring "1.5.0"]
                 [compojure "1.6.0"]

                 ]
  :main ^:skip-aot airtimesender.core
  :omit-source true
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
