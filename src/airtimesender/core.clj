(ns airtimesender.core
  (:require [common.parsers :as par]
            [common.configloader :as config]
            [common.database :as db]
            [common.utilites :as utils]
            [compojure.core :refer :all]
            [compojure.route :as route]
            [clojure.tools.logging :as log]
            [clojure.data.json :as json])
  (:use [org.httpkit.server])
  (:gen-class)
  (:import (java.util.concurrent Executors TimeUnit)))



(declare airtime-sender-routes handle-airtimesend-request process-received-data process-received-registration-data process-get-registered-numbers process-get-success-for-date)

(defonce server (atom nil))

;; this will change to true when we initiate stoping the server
(defonce stop-service (atom false))


(defn stop-server []
  (when-not (nil? @server)
    ;; graceful shutdown: wait 100ms for existing requests to be finished
    ;; :timeout is optional, when no timeout, stop immediately
    (@server :timeout 100)
    (reset! stop-service true)
    (reset! server nil))
  )



(defn handle-airtimesend-request [request]
  (let [{:keys [params]} request
        {:keys [gateway action]} params]
    (log/infof "ParamsRequest(%s)" params)
    (condp = gateway
      "user" (condp = action
                  "login" (process-received-data request)   ; in the case we were to integrate to an interface we will use this
                  "fileload" (process-received-data request) ; this is to load files to the database
                  "logout" (process-received-data request)) ; this logs out a user
      "reports" (condp = action
               "transactions" (process-get-success-for-date request) ; gives all successful and failed transactions "processdate="2015-05-18"
               "registered" (process-get-registered-numbers request)) ; gives a list f registered field officers
      "hr" (condp = action
               "updateregister" (process-received-registration-data request (keyword action)) ; this is for disabling or enabling, deleting field officers
               "createregister" (process-received-registration-data request (keyword action))) ; this is for registering field officers
      (throw (RuntimeException. (format "!unknownGW(%s)" gateway)))
      )
    )
  )

(defn process-received-data [data]
  (json/write-str {:responseCode -1 :responseMsg "Function pending Implementation"})
  )

(defn process-get-success-for-date [request]
  (let [{:keys [processdate status]} (json/read-str (slurp (request :body)) :key-fn keyword)
        data-obtained (db/get-date-transaction processdate status)]
    (json/write-str data-obtained))
  )

(defn process-get-registered-numbers [request]
  (let [{:keys [phoneNumber]} (json/read-str (slurp (request :body)) :key-fn keyword)
        data-obtained (cond (nil? phoneNumber) (db/get-registered-phone-number)
                            :else (db/get-registered-phone-number-status phoneNumber)
                            )]
    (json/write-str data-obtained))

  )

(defn process-received-registration-data [data action]
  (println (format "This is the data %s" [data action]))
  (let [{:keys [phoneNumber  status]} (json/read-str (slurp (data :body)) :key-fn keyword)
        retval (condp =  action

                      :createregister (db/create-field-workers phoneNumber)
                      :updateregister   (db/update-field-workers phoneNumber status))]

    (condp = action
      :createregister  (cond (> retval 0) (json/write-str {:responseCode 0 :responseMsg "Employee Created Successfully."})
                             :else (json/write-str {:responseCode 1 :responseMsg "Employee Creation  Failed."}))

      :updateregister   (cond (> retval 0) (json/write-str {:responseCode 0 :responseMsg "Employee updated Successfully."})
                              :else (json/write-str {:responseCode 1 :responseMsg "Employee update  Failed."}))
      ))
  )


(defroutes airtime-sender-routes
           (POST "/abcd/:gateway/:action" [gateway] handle-airtimesend-request)
           (GET "/abcd/:gateway/:action" [gateway] handle-airtimesend-request))

 (defn watch-csv-directory
  "This function watchs/polls the directory that receives the CSV files with the request to send airtime
  from and when it finds a file passes the validation. It will then call the functions to process the file."
  []
  (log/infof "starting to watch the directory for CSV files.")
      (.scheduleWithFixedDelay (Executors/newScheduledThreadPool 1)
                               (proxy [java.lang.Runnable] []
                                 (run []
                                   (when-not @stop-service
                                     (utils/poll-csv-files)
                                     )))
                               (long 0)
                               (long config/csv-polling-interval)
                               TimeUnit/MILLISECONDS)
  )

(defn -main
  "This is the entry point for the aplication which initialises
  1. loading of configurations

  2. Establishes a  connection to the database

  3. starts an inbuild web server to server requests

  "
  [& args]

  (.addShutdownHook (Runtime/getRuntime) (Thread. stop-server))

  (log/info "#-----------------------------------------------------------------------------------")
  (log/info "ABCD Airtime Sender")
  (log/info "#---------Initialising configuration file--------------------")
  (config/initialize-config)
  (log/info "Configuration file has been successfully initialised")
  (db/initialize-datasource)
  (log/info "Database Configuration has been successfully initialised")

  (par/initialize-subscriber-number-parsers)
  (log/infof "The Phone number parser has been initialised Successfully...")


  (reset! server (org.httpkit.server/run-server #'airtime-sender-routes config/server-options))
  (log/infof "Application started => [%s]" config/server-options)
  (watch-csv-directory)
  )
