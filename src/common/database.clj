(ns common.database
  (:require [clojure.tools.logging :as log]
            [clojure.java.jdbc :as jdbc]
            [common.configloader :as config]
            [clojure.data.json :as json])
  (:import [com.mchange.v2.c3p0 ComboPooledDataSource])
  )

(defn initialize-datasource []
  (log/infof "We start database configurations now ")
  (def db-spec (let [cpds (doto (ComboPooledDataSource.)
                            (.setJdbcUrl config/jdbc-url)
                            (.setUser config/db-user)
                            (.setPassword config/db-password)
                            (.setDriverClass config/jdbc-driver))]
                 {:datasource cpds}))
  (def pooled-db (delay db-spec))
  (log/info "Datasource Initialized.."))
(defn db-connection [] @pooled-db)
(defn validate-phone-entry [phone-nr]
  (jdbc/with-db-connection [conn (db-connection)]
                           (let [sql "select * from tbl_employee_registration where phone_number=?::bigint"
                                 result (first (jdbc/query conn [sql phone-nr]))]
                             (log/infof "This is the results %s" result)
                             (cond (nil? result) false
                                   :else true)
                             )
                           )
)




(defn get-registered-phone-number []
  (jdbc/with-db-connection [conn (db-connection)]
                           (let [sql "select phone_number, case when active_status = 0 then 'Deactivated' else 'active' end active_status from tbl_employee_registration"
                                 result (jdbc/query conn sql)]
                             (log/infof "This is the results %s" result)
                             result)
                           )
  )

(defn get-registered-phone-number-status [phone_number]
  (jdbc/with-db-connection [conn (db-connection)]
                           (let [sql "select phone_number, case when active_status = 0 then 'Deactivated' else 'active' end active_status from tbl_employee_registration where phone_number=?::bigint"
                                 result (jdbc/query conn [sql phone_number])]
                             (log/infof "This is the results %s" result)
                             result)
                           )
  )


(defn create-field-workers [phone-nr]
  (if (false? (validate-phone-entry phone-nr))
  (jdbc/with-db-connection [conn (db-connection)]
                           (let [sql "insert into tbl_employee_registration (phone_number) values(?::bigint) returning request_id"
                                 result (first (jdbc/query conn [sql phone-nr]))]
                             (log/infof "This is the results %s" result)

                             (result :request_id))
                           )
  0)
  )



(defn update-field-workers [phone-nr state]
  (if (false? (validate-phone-entry phone-nr))
    0
    (jdbc/with-db-connection [conn (db-connection)]
                             (let [sql "update tbl_employee_registration set active_status = ?::integer where phone_number=?::bigint returning request_id"
                                   result (first (jdbc/query conn [sql  state phone-nr]))]
                               (log/infof "This is the results %s" result)
                               (cond  (or (nil? result) (empty? result)) 0
                                     :else (result :request_id))
                               )
                             )
    )
  )





(defn save-send-request-to-db [request]
(into []
      (map (fn [save-request]
             (println (format "Now Saving to Database %s" save-request))
             (jdbc/with-db-connection [conn (db-connection)]
                                      (let [sql "insert into tbl_airtime_disburse (request_response)
                              values(?::jsonb) returning request_id"
                                            result (first (jdbc/query conn [sql (json/write-str save-request)]))]
                                        (log/infof "This is the ID of the inserted request %s" result)
                                        (cond (nil? result) 0
                                              :else (result :request_id)))
                                      )

             )
           request)
      )

  )

(defn process-response-to-database [response phone-nr]
  (println (format "The response Phone combination is %s " [response phone-nr]))
    (jdbc/with-db-connection [conn (db-connection)]
                             (let [sql (str "update  tbl_airtime_disburse set request_response = request_response || '"   response   "' where
                                       request_response->>'phoneNumber'=?::text and
                                       request_response->>'status' is NULL and req_time::date=current_date
                                       returning request_id")
                                   result (first (jdbc/query conn [sql  phone-nr]))]
                               (println (format "This is the results %s" result))
                               (cond (nil? result) 0
                                     :else (result :request_id)))
                             )
  )


(defn clear-database-utilities []
    (jdbc/with-db-connection [conn (db-connection)]
                             (let [sql "delete  from tbl_employee_registration;"]
                               (jdbc/execute! conn [sql]))
                             )
    )


(defn clear-db-airtime-requests[]
  (jdbc/with-db-connection [conn (db-connection)]
                           (let [sql "delete  from tbl_airtime_disburse;"]
                             (jdbc/execute! conn [sql]))
                           )
  )



(defn get-date-transaction [date-needed status]
  (jdbc/with-db-connection [conn (db-connection)]
                           (let [sql "select request_response->>'phoneNumber' phonenumber, request_response->>'amount' amount,
                           request_response->>'status' status from tbl_airtime_disburse where req_time::date=?::date and request_response->>'status'=?::text"
                                 result (jdbc/query conn [sql date-needed status])]
                             (log/infof "This is the results %s" result)
                             result)
                           )
  )

