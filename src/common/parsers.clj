(ns common.parsers
  (:require [clojure.tools.logging :as log]
            [clojure.string :as str])
  (:use [common.configloader :onlu (config-value)]))

(defn initialize-subscriber-number-parsers
  "Load some global variables from configuration settings. Then create msisdn matching functions"
  []
  ;; Load values from configuration.

  (def country-code (config-value :country-code))
  (log/infof "country Code %s" country-code)
  (def phonenumber-formats (into []
                                 (map (fn [c]
                                        (let [formats  (str/split c #"\|")]
                                          (assoc {} :nw-digits (Integer/parseInt (nth formats 0))
                                                    :nw-prefix (str (nth formats 1))
                                                    :sno-digits (Integer/parseInt (nth formats 2)))))
                                      (str/split (config-value :phonenumber-no-formats) #","))))


  (log/infof "phonenumber-formats: %s" phonenumber-formats)
  (defn- parse-format-spec [spec]
    (letfn [(grp [name & components]
              (format "(%s)" (apply str components)))]
      (let [{:keys [nw-digits nw-prefix sno-digits] :or {nw-prefix ""}} spec
            sno-matcher (format "\\d{%d}" sno-digits)
            ;; Build network code matcher.
            l1 (- nw-digits (count nw-prefix))
            nw-matcher (condp #(%1 %2 0) l1
                         > (if (= 1 l1) "\\d" (format "\\d{%d}" l1))
                         = ""
                         < (throw (ex-info "Nework prefix longer than allowed network digits."
                                           {:prefix nw-prefix :digits-allowed nw-digits})))]
        [(str (grp "cc") (grp "sno" nw-prefix nw-matcher sno-matcher)) ; bare subscriber number
         (str (grp "cc") "0" (grp "sno" nw-prefix nw-matcher sno-matcher)) ; national format
         (str (grp "cc" country-code) (grp "sno" nw-prefix nw-matcher sno-matcher)) ; international format 1
         (str "\\+" (grp "cc" country-code) (grp "sno" nw-prefix nw-matcher sno-matcher)) ; international format 2
         (str "00" (grp "cc" country-code) (grp "sno" nw-prefix nw-matcher sno-matcher)) ; international format 3
         ]
        )
      )
    )


  (defn- make-phonenumber-parser []
    (let [regexes (reduce (fn [ret spec]
                            `[~@ret
                              ~@(parse-format-spec spec)])
                          []
                          phonenumber-formats)]
      (let [patterns (map #(re-pattern %) regexes)]
        (fn [phone-number]
          (let [phone-number-str (str phone-number)]
            (loop [x (first patterns)
                   xs (next patterns)]
              (let [[match cc sno] (re-matches x phone-number-str)
                    cc (when-not (= "" (str cc)) cc)]
                (if match
                  [{:cc cc :sno sno} :match]
                  (if xs
                    (recur (first xs) (next xs))
                    [{:cc cc :sno sno} :no-match]))))))
        )
      )
    )
  (def %phone-number-parser (make-phonenumber-parser))

  (defn ensure-phone-number [phone-number]
    (let [[{:keys [cc sno]} parse-status] (%phone-number-parser phone-number)]
      (condp = parse-status
        :match    (str (or cc country-code) sno)
        :no-match nil))))


(comment
 "This are functions used for confirming outputs
  the parsing of numbers on the repl"


  (ensure-phone-number"254721803652")

  (ensure-phone-number "254701803652")

  (ensure-phone-number "+254721803652")

  )