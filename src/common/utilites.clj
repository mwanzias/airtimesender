(ns common.utilites
  (:require [clojure.data.json :as json]
            [clojure.tools.logging :as log]
            [common.configloader :as config]
            [common.parsers :as parse]
            [common.database :as db])
(:use clojure.set)
  (:import (java.net URL URLConnection URLEncoder)
           (java.io DataOutputStream InputStreamReader BufferedReader File FilenameFilter)
      ))



(def errors (atom []))

(declare validate-filedata-before-saving prepare-file-for-load process-sending-response call-africas-talking-end process-failed-file)

(defn poll-csv-files
  ""
  []
  (try

    (let [folder (File. (str  config/csv-dump-directory))
    _ (println (format "This is the directory %s " (vec (.listFiles #^File folder))))
          listoffiles (remove nil? (into []
                                         (map (fn [files]
                                                (when (and (.isFile  files) (.endsWith (.getName files) ".csv"))
                                                  files
                                                  )) (.listFiles  folder))))]
      (println (format "The following files have been found %s" listoffiles))
      (map (fn [file-to-process]
             (let [file-name (.getName #^File  file-to-process)
                   processed-file (File. (str config/csv-dump-directory "/processed/" file-name))
                   errored-file (File. (str config/csv-dump-directory "/errored/" file-name))
                   ]
               (println (format "processed-fileocessing file %s" file-to-process))
               (let [file-content (prepare-file-for-load file-to-process)
                     _ (log/infof "filecontent=%s" file-content)
                     passed-validation  (validate-filedata-before-saving file-content)
                     _ (println passed-validation)
                     ]
                 (cond (nil? passed-validation)  (process-failed-file {:origin-file file-to-process
                                                                       :destination-file errored-file}
                                                                      )
                       :else  (process-sending-response
                                (call-africas-talking-end passed-validation)
                                {:origin-file processed-file
                                 :destination-file file-to-process}
                                passed-validation)
                       )
                 )
               )

             ) listoffiles)
      )
    (catch Exception ex
      (println    (format "error %s" (.getMessage ex)))
       (log/errorf   "failed file" ex)
           ))
  )


(defn process-sending-response
  "This function destructures the response.
  picks the array/vector of the responses for each number and
   updates each record into the db with the json response
  "
  [process-response file-options request-to-send]
  (log/debugf "Response from calling endpoint url %s" [process-response (class process-response) file-options])
  (let [total-response (json/read-str process-response :key-fn keyword)
        actual-response (total-response :responses)]
    (cond (empty? actual-response)  (into []
                                          (map (fn [record-response]
                                                 (db/process-response-to-database
                                                   (json/write-str (conj {:status "Failed"} total-response))
                                                   (record-response :phoneNumber))
                                                 ) request-to-send))

          :else  (into []
                       (map (fn [response-destructured]
                              (db/process-response-to-database (json/write-str response-destructured)
                                                               (response-destructured :phoneNumber)
                                                               )
                              )
                            actual-response))
          )

         (let [{:keys [origin-file destination-file]} file-options]
           (.renameTo #^File origin-file destination-file))
    (json/read-str process-response :key-fn keyword)
    )
  process-response)
(defn process-failed-file [file-options]
  (log/infof "Failed File options [%s]" file-options)
  (let [{:keys [origin-file destination-file]} file-options]

    (.renameTo #^File origin-file destination-file))
  )

(defn prepare-file-for-load [filename]
  (let [data-read (clojure.string/split (slurp filename) #"\n")
        first-titles (clojure.string/split (first data-read) #",")
        values-without-titles (rest data-read)]
    (map (fn [record]
           (let [r (clojure.string/split record #",")
                 count-entry (count r)]
             (if (not= count-entry 3)
               (throw (Exception.  (format "The is an incomplete Record => [%s]" r)))
              (dissoc  (rename-keys
                         {(keyword (get first-titles 0)) (get r 0)
                          (keyword (get first-titles 1)) (get r 1)
                          (keyword (get first-titles 2)) (get r 2)}
                         {:Phone-Number :phoneNumber :Amount :amount}
                         )
                       :Employee-Name
                       )
               )
             )
           )
         (vec values-without-titles)
     )
    )
  )
(defn checknumber [s]
  (if (re-find #"^[0.0-9.0\-]+$" s)
    true
    false)
  )


(defn validate-filedata-before-saving
  "This function receives a map of records read from the CSV file
  The function uses the following rules to validate that the record is correct
        1. The phone number to which the airtime is disbursed to needs to be in the database pre registered and active to disable sending to the wrong people
        2. The amount of airtime has got to be within threshhold and correct
        3. A Phone number should appear more than once in the file.
        4. Requests are send to the database if there are no errors detected with the file
  "
  [csv-records]
  (reset! errors [])
      (let [ret-key (into []
                          (map (fn [record]
                                 (println record)
                                 (let [{:keys [phoneNumber amount]} record
                                       test-amount (checknumber  amount)
                                       test-phone-nr (not (nil? (parse/ensure-phone-number phoneNumber)))
                                       number-registered? (db/validate-phone-entry phoneNumber)
                                       ]
                                   (cond (and (true? test-amount) test-phone-nr number-registered?)    {:phoneNumber (str "+" phoneNumber) :amount (str "KES " amount)}
                                         :else (do
                                                 (swap! errors conj   {:data record :amount-validation test-amount :phone-validation test-phone-nr :phone-not-registered number-registered?})
                                                 nil)
                                         )
                                   )
                                 )
                               csv-records)
                          )
            ]
        (if (= 0 (count @errors))
          (let [req-to-send (vec (remove nil? ret-key))]
            (db/save-send-request-to-db req-to-send)
            req-to-send)
          nil
          )
        )
  )



(defn call-africas-talking-end [recipients]
  (try
    (log/infof "africa-talking-url-live = %s" recipients )
        (let [url (URL. config/africa-talking-url-live)
          #^URLConnection connection (.openConnection url)
          _ (doto connection
              (.setDoOutput true)
              (.setRequestProperty "content-type", "application/x-www-form-urlencoded")
              (.setRequestProperty "APikey", config/africa-talking-api-key)
              (.setRequestProperty "Accept", "application/json")
              (.setRequestMethod "POST")
              )
          url-encoded-str (format  "recipients=%s&username=%s"  (URLEncoder/encode (json/write-str recipients))  (URLEncoder/encode config/africa-talking-user-name))
          _ (println (format "Data %s" url-encoded-str))
              data_to_post (.getBytes   url-encoded-str)]
      (let [wr (DataOutputStream. (.getOutputStream connection))
            string-builder (StringBuilder.)]
        (doto wr
            (.write data_to_post)
             (.flush)
          )
        ;(println (format "The http Response is as follows [%s  %s] " (.getResponseCode connection) (.getResponseMessage connection)))
      (let [code (.getResponseCode connection)
            msg (.getResponseMessage connection)]
        (log/infof "This is the response received. %s" [code msg])
                    (cond (and (>= code 200) (<= code 299))
                            (let [in (InputStreamReader. (.getInputStream connection))
                                  bufr (BufferedReader. in)
                                  ]
                                (loop [line ""]
                                 (when (not (nil? line))
                                   (.append string-builder line)
                                   (recur (.readLine bufr))
                                   )
                                  )
                                (.disconnect connection)
                                (println (.toString string-builder))
                                (.toString string-builder)
                                )
                            :else (do
                                    (.disconnect connection)
                                    (println (format "responseCode %s" [code msg]))
                                    (json/write-str {:responseCode code :responseMsg msg})
                                    )
                          )
                    )

        )

      )
    (catch Exception ex
      (log/errorf ex "ResponseCAll")
      (json/write-str {:responseCode -1 :responseMsg "Airtime sending failed"})
      )
    )
  )
(defn check-configs-before-disburse
  "This function receives a map of the records to be disbursed.
  Runs them through the validation rules and disburses airtime to the
  recipients for whom the validation rules have been passed.
  The following rules are considered.

  1. If validation is meant for  file level all records in the file have to pass validation, if one
   record fails we stop the entire process till the file is corrected else the records that are
   correct are processed

  2. if record validation is enabled then the correct records are processed the failed ones are logged.

  3. if user registered validation is enabled, we can only send to people who are already registered in the system
  Else records that are not registered fail to be processed. failed records due to registration validation are logged for further
  analysis and action, registration validation is per record and does not cause entire file to fail.

  4. Validation of duplication : a record may only appear in the file once if a number appears twice then the entire file will fail.
        for duplication check we extract phone numbers from the records to send and form a set out of the numbers, These numbers once their
        count is obtained it needs to be equal to the count of records of the original recipients map short of that means that their are duplicates in the records.
  "

  [map-recipients]
  ;(println (format "Records %s" map-recipients))

  (let [record-count (count map-recipients)
        map-number (into [] (map (fn [record]
                                  (:phoneNumber record))
                               map-recipients))]
    (log/infof " Count Validation %s" [record-count (count (set map-number))])
    (cond (> record-count (count (set map-number))) (do
                                                      (log/infof  "There are some duplicated Records in the file")
                                                      nil)
          :else (do (log/info "Records have passed duplication validation")
                    (cond  (and (or (nil? @errors) (empty? @errors)) (not (empty map-recipients))) (call-africas-talking-end map-recipients)
                           :else (if (and (or (not (empty map-recipients)) (not (nil? map-recipients)))
                                          (= 1                         ;config/file-validation-mode
                                             1))
                                   (do

                                     (cond (>  (count @errors) 0)
                                     (log/warn   (format "The following failed validation %s and was not send" @errors)))
                                     (call-africas-talking-end map-recipients))
                                   (throw (Exception.  (format "Record validation failed! %s \n" {:causes {:errored-records @errors}
                                                                                                  :to-send {:phoneNumber "must be international format"
                                                                                                            :amount "must be in the form KES x"
                                                                                                            }
                                                                                                  }))))
                           )
                    )
          )

    )


  )
