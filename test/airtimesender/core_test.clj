(ns airtimesender.core-test
  (:require [clojure.test :refer :all])
  )
;(common.configloader/initialize-config)
;(common.database/initialize-datasource)
;(common.parsers/initialize-subscriber-number-parsers)
(airtimesender.core/stop-server)
(airtimesender.core/-main)
(deftest a-test

  (testing "test clearing database registered numbers in the case a reset of registration is required."
    (is (>=  (first (common.database/clear-database-utilities)) 0))
    )
  (testing "Creation succeeds hence returns 0 because number already exists"
    (is (>  (common.database/create-field-workers 254721803652) 0))
    )

  (testing "Creation fails hence returns 0 because number already exists"
    (is (=  (common.database/create-field-workers 254721803652) 0))
    )

  (testing "create Field worker, database testing"
    (is (>  (common.database/create-field-workers 254721803663) 0))
    )

  (testing "Check the registered persons for disbursement of airtime"
    (is (>  (count (common.database/get-registered-phone-number)) 0))
    )

  (testing "The functionality for number validation success "
    (is (= true (common.database/validate-phone-entry "254721803652"))))

  (testing "invalid number should be false "
    (is (= false (common.database/validate-phone-entry "25472183652"))))

  (testing "invalid number should be false "
    (is (nil? (common.parsers/ensure-phone-number "254021803652"))))

  (testing "A correct Number should be passed to its value"
    (is (= "254721803652" (common.parsers/ensure-phone-number "254721803652"))))

  (testing "failure due to Duplication records."
   (is (= [{:phoneNumber "+254700123451", :amount "KES 30"} {:phoneNumber "+254701123456", :amount "KES 35"}]
          (common.utilites/validate-filedata-before-saving [{:Employee-Name "Stephen Mwanzia", :phoneNumber "254700123451", :amount "30"}
                                            {:Employee-Name "kiriamiti", :phoneNumber "254701123456", :amount "35"}])
          )
       )
    )

  ;; Testing phone numbers for accuracy
  (testing "failure due to Duplication records."
    (is (nil?  (common.utilites/validate-filedata-before-saving [{:Employee-Name "Stephen Mwanzia", :phoneNumber "254700123451", :amount "30"}
                                             {:Employee-Name "kiriamiti", :phoneNumber "254701123456", :amount "35"}])
           )
        )
    )
  (testing "Should return nil because it is duplicated records"
    (is (nil? (common.utilites/check-configs-before-disburse [{:phoneNumber "+254700123456", :amount "KES 350"}
                                           {:phoneNumber "+254700123456", :amount "KES 350"}]))
        )
    )
  (testing "These records should return nothing because amounts are not correct"
    (is  (let [return-val   (common.utilites/check-configs-before-disburse [{:phoneNumber "+254702123456", :amount "KES 10"}
                                                                            {:phoneNumber "+254701123456", :amount "KES 50"}])]
              (or (= [{:phoneNumber "+254702123456", :amount "KES 10"}
                      {:phoneNumber "+254701123457", :amount "KES 50"}]

                     return-val)

                  (integer? ((clojure.data.json/read-str return-val :key-fn keyword) :responseCode))
                  )

           )
         )
    )
  (testing "Response Codes testing"
    (is (integer? ((clojure.data.json/read-str
                     (common.utilites/call-africas-talking-end
                       [{:phoneNumber "+254701123456", :amount "KES 50"}]) :key-fn keyword) :responseCode)))
     )
  ;;; create test cases to be included with posting to the endpoints







  )

