create database abcd_airtime_distribution;

\c abcd_airtime_distribution;

create table tbl_employee_registration (
phone_number bigint not null,
registration_details jsonb not null,
primary key (phone_number)
);

