(ns common.configloader
  (:require [clojure.tools.logging :as log]
            [clojure.string :as str])
  (:import (java.util Properties)
           (java.io FileInputStream))
  (:use [clojure.walk])
  )

(def app-configs (atom {}))

(defn- config-file []
  (let [result (let [result (or (System/getenv "airtimesender")
                                (System/getProperty "airtimesender")
                                "/home/mwanzias/PERSONAL/jumo-new/source/airtimesender/resources/airtimesender.config"
                                )]
                 (log/infof "This is the configuration file [%s]" result)
                 (when result (java.io.File. result)))]
    (if (and result (.isFile result))
      result
      (do (log/fatal (format "serverConfig(%s) = nil" result))
          (throw (Exception. (format "Server configuration file (%s) not found." result)))))))

(defn load-config [config]
  (let [properties (Properties.)
        fis (FileInputStream. config)]
    ; populate the properties hashmap with values from the output stream
    (.. properties (load fis))
    (keywordize-keys (into {} properties))))





(defn config-value [name & args]
  (let [value (@app-configs name)]
    (if-not (empty? value)
      (let [args (when args (apply assoc {} args))
            {type :type} args
            args (dissoc args :type)
            value (if (vector? value)
                    (loop [x (first value)
                           xs (next value)]
                      (let [properties (dissoc x :value)]
                        (if (or (and (empty? args)
                                     (empty? properties))
                                (and (not (empty? args))
                                     (every? (fn [[k v]]
                                               (= (properties k) v))
                                             args)))
                          (x :value)
                          (when xs
                            (recur (first xs) (next xs))))))
                    value)]
        (when value
          (let [value #^String value]
            (cond (or (nil? type) (= type :string)) value
                  ;; ---
                  (= type :int) (Integer/valueOf value)
                  (= type :long) (Long/valueOf value)
                  ;(= type :integer) (resize-int (Long/valueOf value))
                  (= type :bool) (contains? #{"yes" "true" "y" "t" "1"}
                                            (.toLowerCase value))
                  (= type :keyword) (keyword value)
                  (= type :path) (java.io.File. value)
                  (= type :url) (java.net.URL. value)))))
      (if-let [args (when args (apply assoc {} args))]
        (args :default)
        nil))))




(defn initialize-config []
  (log/info "Initializing configurations..")
  (reset! app-configs (load-config (config-file)))
  (log/debugf "configs=%s" @app-configs)

  ;; Datasource settings
  (def jdbc-driver (config-value :jdbc-driver))
  (def jdbc-protocol (config-value :jdbc-protocol))
  (def jdbc-url (config-value :jdbc-url))
  (def db-user (config-value :db-user))
  (def db-password (config-value :db-password))
  (def server-port (config-value :server-port))


  ;security parameters for mpesa keys
  (def africa-talking-api-key  (config-value :africa-talking-api-key))
  (def africa-talking-user-name (config-value :africa-talking-user-name))
  (def africa-talking-url-live (config-value :africa-talking-url-live))
  (def africa-talking-url-sandbox (config-value :africa-talking-url-sandbox))
  (def file-validation-mode (config-value :file-validation-mode))
  (def csv-polling-interval (Integer/parseInt (config-value :csv-polling-interval)))
  (def csv-dump-directory  (config-value :csv-dump-directory))
  (def file-process-counter (let [vals (config-value :file-process-counter)]
                              (Integer/parseInt vals))
    )
  ;; Service Configurations

  (def server-options (hash-map
                        :port (if (nil? server-port)
                                (config-value :server-port) ;; default port
                                (Integer/parseInt (str  server-port)))
                        :thread (if (nil? (System/getProperty "proxy.server.threads"))
                                  200 ;; default port
                                  (Integer/parseInt (System/getProperty "proxy.server.threads")))
                        :worker-name-prefix (if (nil? (System/getProperty "proxy.server.worker-name-prefix"))
                                              "worker-" ;; default port
                                              (System/getProperty "proxy.server.worker-name-prefix"))
                        :queue-size (if (nil? (System/getProperty "proxy.server.queue-size"))
                                      20000 ;; default port
                                      (Integer/parseInt (System/getProperty "proxy.server.queue-size")))
                        :max-body (if (nil? (System/getProperty "proxy.server.max-body"))
                                    8388608 ;; default port
                                    (Long/parseLong (System/getProperty "proxy.server.max-body")))
                        :max-line (if (nil? (System/getProperty "proxy.server.max-line"))
                                    4096 ;; default port
                                    (Integer/parseInt (System/getProperty "proxy.server.max-line")))))

  (def server-shutdown-timeout (if (nil? (System/getProperty "proxy.server.shutdown-timeout"))
                                 1000
                                 (Integer/parseInt (System/getProperty "proxy.server.shutdown-timeout"))))
  )
